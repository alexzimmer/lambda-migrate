FROM golang:1.15.6 as builder
WORKDIR /workdir
ADD . .
RUN go build -o /main

# Copy the compilate into the target image
FROM public.ecr.aws/lambda/provided:al2
COPY --from=builder /main /main
ENTRYPOINT [ "/main" ]