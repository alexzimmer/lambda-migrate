# Migrate

## Project Description

This is a dead simple wrapper around the [migrate tool](https://github.com/golang-migrate/migrate) by to run it on Lambda. 
I use this tool to migrate my Aurora Database via a Lambda that's triggered in my build process.
This way, I don't have to waste time on Lambda cold start to perform migrations.

## Usage

> The main image is configured to be used with Gitlab and Postgres.
> If your use case is a different, you need to clone this repo and change the imports.

1. Mirror the image `registry.gitlab.com/alexzimmer/lambda-migrate:latest` into your Elastic Container Registry
2. Create a Lambda from this image. I've named my `migrate`
3. Invoke the Lambda function like this:

```shell
aws lambda invoke --function-name migrate \
  --payload '{"source_url":"GITLAB URL", "db_connection_string":"POSTGRES URL}' \
  response.json
```