package main

import (
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/gitlab"
)

type MigrationEvent struct {
	Source             string `json:"source_url"`
	DBConnectionString string `json:"db_connection_string"`
}

func main() {
	lambda.Start(MigrationHandler)
}

func MigrationHandler(event MigrationEvent) error {
	m, err := migrate.New(event.Source, event.DBConnectionString)

	if err != nil {
		return fmt.Errorf("error while initializing migration client: %w", err)
	}

	if err := m.Up(); err != nil {
		if err != migrate.ErrNoChange {
			return fmt.Errorf("error while running migrations: %w", err)
		}
	}

	return nil
}
