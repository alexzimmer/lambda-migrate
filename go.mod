module migration

go 1.15

require (
	github.com/aws/aws-lambda-go v1.22.0
	github.com/golang-migrate/migrate/v4 v4.14.1
)
